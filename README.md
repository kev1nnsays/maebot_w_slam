* Instructors:
    + Prof. Ella Atkins <ematkins@umich.edu>
    + Prof. Shai Revzen <shrevzen@umich.edu>
* Other community or team contact:
    + Lab Engineer: Mr. Peter Gaskell <pgaskell@umich.edu>
* Team: 
	+ Kevin Choi
	+ John Brooks
	+ Ripudaman Arora

# Video Results: #

https://www.youtube.com/playlist?list=PLo79OivWOV6egtnivBLZsRvndhm2NfpPv